package task01

/* Bináris keresőfát fogunk implementálni.
 * - bináris fa: minden csúcsának lehet egy "bal" és egy "jobb" fia
 * - lesz egy "üres fa", ami már nem tárol adatot és nincsenek gyerekei
 * - a többi fa mind BinaryTree, bal gyerekkel, jobb gyerekkel és egy Int adatmezővel
 * - attól keresőfa, hogy a bal gyerekben az összes tárolt adat mindig kisebb, mint a gyökérben
 *   és a jobb gyerekben az összes tárolt adat mindig nagyobb, mint a gyökérben
 * - Int-ek halmazát lehet benne rendezetten tárolni
 **/

trait IntTree {
  /* Implementáljunk egy "contains" metódust, ami megmondja, hogy a fában valahol szerepel-e a megadott adat!
   * Pl. BinaryTree( BinaryTree( EmptyTree, 3, EmptyTree ), 5, EmptyTree )-re a contains(3) és a contains(5) true,
   * minden másra false.
   **/
  def contains(value: Int): Boolean
  
  /* Implementáljunk egy + metódust, ami visszaad egy új fát, ami annyiban különbözik az eredetitől, hogy
   * ha az eredetiben nincs benne a value, akkor beleteszi azt is! (ha benne van, akkor ne változzon a fa)
   * Pl. EmptyTree + 3 értéke BinaryTree(EmptyTree, 3, EmptyTree) legyen,
   * BinaryTree(EmptyTree, 3, EmptyTree) + 5 értéke pedig (mondjuk) BinaryTree(EmptyTree, 3, BinaryTree(EmptyTree, 5, EmptyTree)).
   * note: ehhez a teszthez szükséges egy helyesen működő contains metódus is.
   **/
  def +(value: Int): IntTree
   
   /* Implementáljunk egy foreach metódust, ami kap egy f: Int => Any függvényt és minden, a fában szereplő adaton
    * kiértékeli f-et! Visszatérési értéke legyen Unit.
    **/
  def foreach(f: Int => Any): Unit
  
  /* Implementáljunk egy exists metódust, ami kap egy p: Int => Boolean predikátumot és visszaadja, hogy szerepel-e
   * a fában legalább egy olyan data érték, melyre igaz p!
   **/
  def exists(p: Int => Boolean): Boolean
  
  /* Implementáljunk egy count metódust, ami kap egy p: Int => Boolean predikátumot és visszaadja, hogy a fában
   * mennyi olyan adat van, melyre igaz p!
   **/
  def count(p: Int => Boolean): Int
  
  /* Implementáljunk egy fold metódust, ami kap egy kezdőértéket és egy f:(Int,Int) -> Int függvényt, és
   * növekvő sorrendben bejárva a fát, foldolja az értéket!
   * pl. fold(0)(_+_) összegezze a fában az adatok értékeit.
   **/
  def fold[T](base: T)(op: (T, Int) => T): T

  /* Implementáljunk egy ++ metódust, ami visszaadja this és that unióját!
   * note: ehhez a teszthet kell egy működő + operátor és egy működő contains metódus.
   **/
  def ++(that: IntTree): IntTree
  
  /* Implementáljunk egy map metódust, ami kap egy f: Int => Int függvényt és előállít egy olyan IntTree-t,
   * melyben az összes olyan f(data) szerepel, ami data benne volt a fában!
   * note: ehhez a teszthez kell egy működő contains metódus.
   **/
  def map(f: Int => Int): IntTree
  
  /* Implementáljunk egy filter metódust, ami kap egy p: Int => Boolean predikátumot és előállít egy olyan IntTree-t,
   * melyben pont azok az elemek szerepelnek, akik szerepeltek a fában és igaz rájuk p!
   **/
  def filter(p: Int => Boolean): IntTree

  /* Ahhoz, hogy if guarddal forduljon a comprehension (line 141), kell egy "withFilter" metódus ami egyelőre legyen maga a "filter"
   * Később majd látjuk, hogy milyen különbségeket "illik" tenni a kettő közt
   **/
  def withFilter: (Int => Boolean) => IntTree = filter

}

/* Ha úgy látjuk jónak, a fenti függvények implementációit tehetjük a leszármazott osztályokba is.
 **/
case object EmptyTree extends IntTree {
  override def contains(value: Int): Boolean = false
  
  override def +(value: Int): IntTree = BinaryTree(EmptyTree,value,EmptyTree)

  override def foreach(f: Int => Any): Unit = ()

  override def exists(p: Int => Boolean): Boolean = false

  override def count(p: Int => Boolean): Int = 0

  override def fold[T](base: T)(op: (T, Int) => T): T = base

  override def ++(that: IntTree): IntTree = that

  override def map(f: Int => Int): IntTree = this

  override def filter(p: Int => Boolean): IntTree = this
}

case class BinaryTree(left: IntTree, data: Int, right: IntTree) extends IntTree {
  override def contains(value: Int): Boolean =
    if (value == data) true
    else if (value < data) left contains value
    else right contains value

  override def +(value: Int): IntTree =
    if (value == data) this
    else if (value < data) BinaryTree(left + value, data, right)
    else BinaryTree(left, data, right + value)

  override def foreach(f: Int => Any): Unit = {
    for (data <- left) f(data)
    f(data)
    for (data <- right) f(data)
  }

  override def exists(p: Int => Boolean): Boolean = 
    p(data) || (left exists p)  || (right exists p)
 
  override def count(p: Int => Boolean): Int = 
    (left count p) + (right count p) + (if (p(data)) 1 else 0)

  override def fold[T](base: T)(op: (T, Int) => T): T = {
    val leftFolded = left.fold(base)(op)
    val midFolded = op(leftFolded, data)
    right.fold(midFolded)(op)
  }

  override def ++(that: IntTree): IntTree =
    that.fold[IntTree](this)( _ + _ )

  override def map(f: Int => Int): IntTree = 
    fold[IntTree](EmptyTree)( _ + f(_) )

  override def filter(p: Int => Boolean): IntTree = 
    fold[IntTree](EmptyTree)( (acc,data) => if (p(data)) acc + data else acc )

}

// ezekhez nincsenek tesztek, ha eljutunk idáig, írjunk pár saját tesztet is!
object TreeApp extends App {
  /* Írjunk olyan függvényt, mely kiírja konzolra a kapott IntTree-beli páros számokat!
   **/
  def printOddValues(tree: IntTree): Unit =
    for (data <- tree if (data % 2 == 0) ) println(data)
  
  /* Írjunk olyan függvényt, mely az input IntTree-ből készít egy olyat, melyben az eredeti
   * fa-beli adatok ellentettjei vannak!
   **/
  def changeSigns(tree: IntTree): IntTree = tree map { _ * -1 }
  
  /* Írjunk olyan függvényt, mely az input List[Int] elemeit (balról jobbra) egyesével beszúrja egy
   * IntTree-be, és adjuk vissza az eredményként kapott IntTree-t!
   **/
  def listToTree(list: List[Int]): IntTree = 
    list.foldLeft[IntTree](EmptyTree)( _ + _ )
}
