package task02

/**
 * A task: implementáljunk egy funkcionális generikus HashHalmaz[T] -t.
 *
 * Egy ilyen adatstruktúrában
 * - T típusú objektumokat tárolunk
 * - "halmaz", tehát mindből max egy van az adatszerkezetben
 * - használjuk a T típusú objektumok hashCode metódusát (ez a metódus minden osztályban létezik, egy intet ad vissza)
 * - a HashHalmaz egy hashVector: Vector[Vector[T]]-ben tárolja az adatot, a külső vektornak van egy size: Int mérete
 * - beszúráskor: a beszúrandó objektum hashCode-ját kiszámoljuk, vesszük mod size (0 és size-1 közt legyen az értéke;
 *   vigyázat: a hashCode adhat negatívat is), és ha így az `i` indexet kapjuk, akkor a hashVector[i]-ben először is
 *   megnézzük, hogy ott van-e már a beszúrandó érték, ha már benne van, akkor nem változtatunk semmit, ha nincs,
 *   akkor ennek a hashVector[i]-nek a végére szúrjuk be;
 * - ha beszúrás után a hashVector-beli celláknak több, mint a 3/4-ében nemüres az ott levő vektor, akkor
 *   egy kétszeres méretű hashHalmaz-t építük, beleszúrjuk az elemeket ebből a vektorból, majd megint ellenőrizzük
 *   az elemszámot és addig bővítünk duplázásokkal, amíg már legfeljebb a 3/4-e nemüres az entryknek
 * - contains: megnézzük, hogy a hashCode-nak megfelelő vektorban szerepel-e a lekérdezett érték
 * - remove: úgy vesszük ki a vektorból, ha benne van egy elem, hogy a vektor utolsó elemével updateljük
 *   a törlendő pozíciót, majd eldobjuk a vektor utolsó elemét.
 *
 * - foreach, map, foldLeft a szokásos módon, járja be az elemeket úgy, hogy először a 0. vektort balról jobbra,
 * majd az 1. vektort, ..., végül a size - 1. vektort.
 *
 * Ezekhez helper függvényeket is implementálunk.
 *
 * A scala API doksiból érdemes lehet a Vector[T] osztály
 * +:(T): Vector[T], :+(T): Vector[T]
 * updated(Int,T): Vector[T]
 * dropRight(Int): Vector[T]
 * contains, filter
 * metódusait megnézni hozzá
 */

object VectorUtils {
  /**
   * Implementáljunk egy getIndexFrom függvényt, mely kap egy v vektort, egy e értéket és egy i indexet,
   * és visszaadja Some(j)-t, ha j az első olyan index, melyre i <= j és v(j)=e, és None-t, ha nincs ilyen index!
   * (azaz, ha az érték nem szerepel i utáni indexen)
   *
   *
   */
  def getIndexFrom[T](v: Vector[T], e: T, i: Int): Option[Int] = ???

  /**
   * Implementáljunk egy getIndex függvényt, mely kap egy v vektort és egy e értéket, és visszaadja Some(i)-t, ha
   * i az első olyan index, melyre v(i)=e, és None-t, ha nincs ilyen index! (azaz ha az érték nem szerepel a vektorban)
   */
  def getIndex[T](v: Vector[T], e: T): Option[Int] = ???

  /**
   * Implementáljunk egy removeSwap függvényt, mely kap egy v vektort és egy e értéket, és
   * - ha v-ben nem szerepel e, akkor visszaadja az eredeti v-t,
   * - ha szerepel benne, akkor az *első* előfordulás helyére beteszi a vektor utolsó elemét, levágja az utolsó elemet
   * (így effektíve kitörli az érték első előfordulását a vektorból), és visszaadja az eredményként kapott új vektort!
   * (van egy dropRight metódus, ami ehhez hasznos lehet.)
   */
  def removeSwap[T](v: Vector[T], e: T): Vector[T] = ???
}

  /**
   * A HashHalmaz osztályunk: egy case class, van benne egy vektor és tudja a "külső" vektor méretét.
   * Tartozik hozzá egy companion object.
   */
case object HashHalmaz {
    /**
     * Implementáljuk az apply metódust úgy, hogy a HashHalmaz[T] hívás egy üres, size = 8 mérettel inicializált
     * külső vekttorral térjen vissza! Tehát egy
     * Vector(Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector(),Vector())-al legyen a data mező inicializálva.
     */
    def apply[T](): HashHalmaz[T] = ???
    /**
     * Implementáljuk az apply(size: Int) metódust úgy, hogy a HashHalmaz[T] hívás egy üres, size mérettel inicializált
     * külső vekttorral térjen vissza! Tehát egy
     * Vector(Vector(),...Vector())-al, összesen size darab Vector()-ral, legyen a data mező inicializálva.
     */
    def apply[T](size: Int): HashHalmaz[T] = ???
}

case class HashHalmaz[T](data: Vector[Vector[T]], size: Int) {
    /**
     * Implementáljuk a loadFactor metódust, mely visszaadja, hogy a data vektor elemeinek mekkora része
     * nemüres vektor! (tehát ez egy 0 és 1 közti Double érték lesz.)
     */
    def loadFactor: Double = ???
    /**
     * Implementáljuk az offset metódust, mely megkap egy e értéket,
     * - veszi az e hashCode értékét
     * - ennek a size-al vett osztási maradékát (pl. ha size 8, és hashCode = 15, akkor ez az érték 7; ha hashCode = -3, akkor ez az érték 5)
     * ami így tehát 0..size-1 közti Int lesz,
     * és ezt az intet adja vissza!
     */
    def offset(e: T): Int = ???

    /**
     * Implementáljuk a contains(e: T): Boolean metódust, mely
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát (pl. ha size 8, és hashCode = 15, akkor ez az érték 7; ha hashCode = -3, akkor ez az érték 5)
     * - és a data ennyiedik vektorában keresi meg az e értéket, és lejelenti, tartalmazza-e!
     */
    def contains(e: T): Boolean = ???

    /**
     * Implementáljuk az insert(e: T): HashHalmaz[T] metódust, mely a contains-nak megfelelő helyre szúrja az e értéket:
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát
     * - ha a data ennyiedik vektorában már szerepel az e érték, akkor nem készít új példányt, visszaadja módosítás nélkül az adatszerkezetet,
     * - ha még nem szerepel, akkor beszúrja ennek a vektornak a végére (funkcionálisan),
     * - ezen kívül ha egy üres vektorba szúrt be most értéket, akkor még a resizeIfNeeded-et is hívja az eredményre,
     * - és visszaadja az új HashHalmazt!
     */
    def insert(e: T): HashHalmaz[T] = ???

    /**
     * Implementáljuk a resizeIfNeeded metódust, mely
     * - ha a data -beli nemüres vektorok aránya 0.75 fölött van, akkor
     *   - létrehoz egy új HashHalmaz-t, kétszer akkora size-al, mint az aktuális,
     *   - ebbe beszúrja az aktuális halmazunk összes elemét,
     *   - és az eredményre is hívja a resizeIfNeeded metódust, ennek az eredményét adja vissza!
     * - ha pedig 0.75 vagy kisebb, akkor csak visszaadja módosítás nélkül az adatszerkezetet!
     */
    def resizeIfNeeded: HashHalmaz[T] = ???

    /**
     * Implementáljuk a remove(e: T): HashHalmaz[T] metódust, mely
     * - veszi az e objektum hashCode értékét
     * - ennek a size-al vett osztási maradékát
     * - ha a data ennyiedik vektorában nem szerepel az e érték, akkor visszaadja a hashhalmazt módosítás nélkül,
     * - egyébként pedig az e első előfordulását kiveszi ebből a vektorból oly módon, hogy a hátsót szúrja be a helyére,
     * - elkészít egy új hashhalmazt ezzel a módosított tartalommal,
     * és ezt adja vissza!
     */
    def remove(e: T): HashHalmaz[T] = ???

    /**
     * Implementáljunk egy foreach metódust, ami sorban végigmegy az i=0..size-1-edik vektorokon a data-n belül,
     * ezeknek mindnek az elemein és végrehajtja mindegyik elemen a kapott f függvényt!
     */
    def foreach[U](f: T => U): Unit = ???

}
